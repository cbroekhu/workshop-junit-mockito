package solutions.workshop.june8.calculation;

import com.workshop.june8.calculation.InterestCalculator;
import com.workshop.june8.calculation.exception.CalculationException;
import com.workshop.june8.calculation.exception.EndpointException;
import com.workshop.june8.calculation.webservice.BankingService;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

//@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
public class CalculationTest {

    String now = "20200608";

    @InjectMocks
    InterestCalculator interestCalculator;

    @Mock
    BankingService bankingService;

    @Test
    public void calculateInterestTest() throws EndpointException, CalculationException {

        when(bankingService.getInterestRate(now)).thenReturn(0.04D);

        assertEquals( 1771.45D, interestCalculator.calculateInterest( 1400D, 5));

    }

    @Test
    public void calculateInterestWithDateTest() throws EndpointException, CalculationException {

        String date = "20170608";
        when(bankingService.getInterestRate( "20170608")).thenReturn(0.02D);

        assertEquals( 1034.89D, interestCalculator.calculateInterest( 800D, 12, date));

    }

    @Test
    public void calculateInterestWithException() throws EndpointException {

        String date = "20150312";

        when(bankingService.getInterestRate(date)).thenThrow( new EndpointException("Endpoint not available"));

        Exception exception = Assertions.assertThrows(CalculationException.class, () -> {
            interestCalculator.calculateInterest( 800D, 12, date);
        });

        assertEquals("Calculation failed due to: Endpoint not available", exception.getMessage());

    }

}
