package com.workshop.june8.controller;

import com.workshop.june8.controller.model.Article;
import com.workshop.june8.controller.repositories.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api")
public class ArticleController {

//    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    public ArticleController( ArticleRepository articleRepository){

        this.articleRepository = articleRepository;
    }

    @GetMapping(value = "/article/getArticleById/{id}", produces = "application/json")
    public ResponseEntity<Article> getArticleById(@PathVariable final long id) {

        Article article = articleRepository.findById(id).orElse(null);

        // Business logic to test, lower stock by one
        if(article.getStock() > 0){
            article.setStock( article.getStock() -1);
        }

        return ResponseEntity.ok(article) ;

    }

    @GetMapping(value = "/article/getAllOddArticles", produces = "application/json")
    public ResponseEntity<Iterable<Article>> getAllArticles() {

        Iterable<Article> articles = articleRepository.findAll();

        // Business logic to test
        // limit the list to three articles
        articles = removeEvenIds(articles);

        return ResponseEntity.ok(articles) ;

    }

    @PostMapping(value = "/article/addArticle", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Article> addArticle( @RequestBody final Article articleIn) {

        Article articleOut = articleRepository.save(articleIn);

        // Business logic to test
        // Set initial stock to 1000
        articleIn.setStock(1000);

        return ResponseEntity.ok(articleOut) ;

    }

    private Iterable<Article> removeEvenIds( Iterable<Article> in){

        List<Article> unevenList = StreamSupport.stream(in.spliterator(), false)
                .filter( s -> s.getId() % 2 != 0)
                .collect(Collectors.toList());

        return( (Iterable)unevenList);

    }

}
